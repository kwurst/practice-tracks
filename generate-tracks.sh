#!/usr/bin/env bash

# $1 is the directory where all tracks live (absolute path)
# $2 is the name of the directory for the track
# Original mp3 must have the same name as the track directory

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "${SCRIPT_DIR}"
ALL_TRACKS_DIR=$1
TRACK_DIR=$2
TRACK_NAME=$2
TRACK_FILE="$TRACK_NAME.mp3"

#mkdir "./spleeter/input/$TRACK_FILE"
# Copy source mp3 to spleeter input directory
cp "$ALL_TRACKS_DIR/$TRACK_DIR/$TRACK_FILE" "./spleeter/input/$TRACK_FILE"

# Separate source mp3 into stems
docker run \
    -v ./spleeter:/spleeter \
    -e MODEL_PATH=spleeter/model/ \
    deezer/spleeter:3.8 separate \
        -p spleeter:5stems \
        -o spleeter/output/ \
        "spleeter/input/$TRACK_FILE"

# Separated stems are in spleeter output directory
cd "spleeter/output/$TRACK_DIR"

# Build variant tracks from separated stems
#   Spawn each as a child processes to speed things up

# Convert single stems to mp3
sox vocals.wav "V____-$TRACK_NAME.mp3" &
sox bass.wav "_B___-$TRACK_NAME.mp3" &
sox drums.wav "__D__-$TRACK_NAME.mp3" &
sox other.wav "___O_-$TRACK_NAME.mp3" &
sox piano.wav "____P-$TRACK_NAME.mp3" &

# Generate mp3s with 2 stems
sox -m vocals.wav bass.wav "VB___-$TRACK_NAME.mp3" &
sox -m vocals.wav drums.wav "V_D__-$TRACK_NAME.mp3" &
sox -m vocals.wav other.wav "V__O_-$TRACK_NAME.mp3" &
sox -m vocals.wav piano.wav "V___P-$TRACK_NAME.mp3" &
sox -m bass.wav drums.wav "_BD__-$TRACK_NAME.mp3" &
sox -m bass.wav other.wav "_B_O_-$TRACK_NAME.mp3" &
sox -m bass.wav piano.wav "_B__P-$TRACK_NAME.mp3" &
sox -m drums.wav other.wav "__DO_-$TRACK_NAME.mp3" &
sox -m drums.wav piano.wav "__D_P-$TRACK_NAME.mp3" &
sox -m other.wav piano.wav "___OP-$TRACK_NAME.mp3" &

# Generate mp3s with 3 stems
sox -m drums.wav other.wav piano.wav "__DOP-$TRACK_NAME.mp3" &
sox -m bass.wav other.wav piano.wav "_B_OP-$TRACK_NAME.mp3" &
sox -m bass.wav drums.wav piano.wav "_BD_P-$TRACK_NAME.mp3" &
sox -m bass.wav drums.wav other.wav "_BDO_-$TRACK_NAME.mp3" &
sox -m vocals.wav other.wav piano.wav "V__OP-$TRACK_NAME.mp3" &
sox -m vocals.wav drums.wav piano.wav "V_D_P-$TRACK_NAME.mp3" &
sox -m vocals.wav drums.wav other.wav "V_DO_-$TRACK_NAME.mp3" &
sox -m vocals.wav bass.wav piano.wav "VB__P-$TRACK_NAME.mp3" &
sox -m vocals.wav bass.wav other.wav "VB_O_-$TRACK_NAME.mp3" &
sox -m vocals.wav bass.wav drums.wav "VBD__-$TRACK_NAME.mp3" &

# Generate mp3s with 4 stems
sox -m bass.wav drums.wav other.wav piano.wav "_BDOP-$TRACK_NAME.mp3" &
sox -m vocals.wav drums.wav other.wav piano.wav "V_DOP-$TRACK_NAME.mp3" &
sox -m vocals.wav bass.wav other.wav piano.wav "VB_OP-$TRACK_NAME.mp3" &
sox -m vocals.wav bass.wav drums.wav piano.wav "VBD_P-$TRACK_NAME.mp3" &
sox -m vocals.wav bass.wav drums.wav other.wav "VBDO_-$TRACK_NAME.mp3" &

wait # wait for them all to finish

# Clean up generated stems and source mp3
rm bass.wav &
rm drums.wav &
rm other.wav &
rm vocals.wav &
rm piano.wav &
rm "../../../spleeter/input/$TRACK_FILE" &

# Move generated mp3s to source mp3 file's directory
mv *.mp3 "$ALL_TRACKS_DIR/$TRACK_DIR/" &

# wait for mv to finish before deleting mp3 files
wait $!  # wait for last process to be spawned
rm -rf "../../../spleeter/output/$TRACK_DIR"