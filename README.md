# Splitting MP3s into "Practice Tracks"

This project can be used to split an MP3 file into "practice tracks" for a
band.

[Deezer Spleeter](https://github.com/deezer/spleeter) is used to split an
mp3 file into 5 stems - vocal, bass, drums, piano, other. Then these stems
are recombined into all 30 possible combinations

* Individual mp3 tracks for each stem
* mp3 tracks for all combinations of 2 stems
* mp3 tracks for all combinations of 3 stems
* mp3 tracks for all combinations of 4 stems

## Requires

* Docker to run the containerized version of Deezer Spleeter
* [SoX](https://sox.sourceforge.net/) to merge stems and convert to mp3

## Using

This program expects that you have, somewhere on your computer, a directory structure like this:

```
practice-tracks
  track1
    track1.mp3
  track2
    track2.mp3
  ...
```

Each individual track has a directory containing an mp3. The directory and the filename are the same.

Once the program runs, the track directory will have an additional 30 mp3 files named like this:

```
VBDOP-track.mp3
V____-track.mp3
...
_BD__-track.mp3
...
```

The letters represent

* V - Vocals
* B - Bass
* D - Drums
* O - Other
* P - Piano

If the file has the letter in the filename, then that stem is included in the track. An underscore means that the stem has not been included.

Run the program as follows:

```
./generate-tracks.sh <practice tracks directory> <name of track to generate>
```
The <practice tracks directory> should be an absolute path to the top-level directory for your practice tracks. See the expected directory structure above.

Use double quotes around the path and/or track name if either or both contain spaces or special characters.

For example:

```
./generate-tracks.sh "/home/username/practice-tracks" "track 1"
```

## Scratch Space

The `spleeter` directory is used by the script as a scratch space. It is mounted as a volume by the spleeter docker image.

Copyright © 2025 Karl R. Wurst.

<!-- markdownlint-disable MD033 -->
<img src="https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit [http://creativecommons.org/licenses/by-sa/4.0/](http://creativecommons.org/licenses/by-sa/4.0/) or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.
